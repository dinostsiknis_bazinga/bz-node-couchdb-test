// routes/developments/units.js
//
// Routes for development units
// 

// This will initialize the DB and import all model prototypes
var Models = require('../../models');

//------------------------------------------------------------------------------

module.exports = function attachHandlers(router) {

    // Get index
    router.get('/api/developments/:id/units', listDevelopmentUnits);
};

//------------------------------------------------------------------------------

function listDevelopmentUnits(req, res, next) {

    // Query the unit model
    Models.Unit.find({ keys: [parseInt(req.params.id)] }, function(e, result) {
        if (e) {
            return next(e);
        }

        res.send(result);
    });
};

//------------------------------------------------------------------------------
