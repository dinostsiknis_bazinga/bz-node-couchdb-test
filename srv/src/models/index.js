// models/index.js
//

// Init the ORM
//var mongoskin = require('mongoskin');
var couch = require('nano')('http://localhost:5984');

// Build the database name
// NOTE: This is a termporary hack until I figure out configuration
if (process.env.NODE_ENV === undefined) {
    process.env.NODE_ENV = '';
}

//var dbName = 'test' + process.env.NODE_ENV;

// Init db connection
//var db = mongoskin.db('mongodb://@localhost:27017/' + dbName, { safe: true});
var db = couch.db.use('test');

// Init each model with the db connection
//var MarketIndicator = require('./marketindicators');
var Unit            = require('./units');

// Export all initialized ORM classes
module.exports.Unit = new Unit(db);
//module.exports.MarketIndicator = new MarketIndicator(db);

