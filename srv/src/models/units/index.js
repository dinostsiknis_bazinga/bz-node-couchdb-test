//
// models/units/index.js
//
// Data access for units
//

//------------------------------------------------------------------------------

var Unit = module.exports = function(db) {

    // Access to units model through input database
    this.units = db;
};

//------------------------------------------------------------------------------

Unit.prototype.find = function(query, callback) {
    // DEBUG
    debugger;
    // END DEBUG

    this.units.view('units', 'by_development', query, function(err, results) {
        // DEBUG
        //debugger;
        // END DEBUG
        return callback(err, results);
    });

    //this.units.find(query, function(err, results) {
    //    return results.toArray(callback);
    //});
};

//------------------------------------------------------------------------------
